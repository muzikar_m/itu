const express = require("express");
const schedule = require("node-schedule");
const phantom = require("phantom");
const bodyParser = require("body-parser");
const fs = require("fs");
const app = express();
const uuid = require("uuid/v4");
const sendmail = require("sendmail");
const hbs = require("express-handlebars");
const port = 3000;

let db = JSON.parse(fs.readFileSync("data/db.json"));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.engine("handlebars", hbs({
    partialsDir: "views/partials"
}));
app.set("view engine", "handlebars");


app.get("/dbtest", (req, res) => {
    jobChecker.invoke();
    res.send();
});
app.post("/check", (req, res) => {
    let data = req.body;
    if (!data.id){
        data.id = uuid();
    }
    db.checks.push(data);
    res.status(201).send();
    jobChecker.invoke();
});

app.patch("/check", (req, res) => {
    let data = req.body;
    let it = db.checks.find((val) => val.id === data.id);
    Object.keys(data).forEach((key) => {
        it[key] = data[key];
    });
    res.send(JSON.stringify(it));
});

app.get("/check", (req, res) => {
    let id = req.params["id"] || -1;
    if (id == -1){
        res.send(db.checks);
    } else {
        const check = db.checks[id] || {};
        res.status(check === {} ? 404 : 200).send(JSON.stringify(check));
    }
})

app.use(express.static("public"));

app.get("/", (req, res) => {
    res.render("home", {title: "test", checks: db.checks});
});

app.listen(port, () => console.log(`Server running on port ${port}`));

const htmlEq = (val1, val2) => {
    return val1 === val2;
}

const notificationHandlers = {
    email: (check) => {
        console.log(`Sending mail to ${JSON.stringify(check)}`);
        /*sendmail({
            from: "notifier@itu.com",
            to: check.notifications.email.to,
            subject: `${check.url} has changed`,
            html: `${check.url} has changed from ${check.val}`
        });*/
    },
    blank: (check) => {
        console.error(`${JSON.stringify(check)} NOT IMPLEMENTED YET`);
    }
}

const notify = (check) => {
    Object.keys(check.notifications).forEach((key) => notificationHandlers[key in notificationHandlers?key:"blank"](check));
}

const checkWebsite = async (check) => {
    const instance = await phantom.create();
    const page = await instance.createPage();
    console.log(`current check: ${JSON.stringify(check)}`);
    page.open(check.url).then(function (status) {
        console.log(`Website ${check.url} status: ${status}`);
        if (status === "success"){
            page.includeJs("http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js").then(function (){
                page.evaluate(function (check) {
                    return $(check.selector).text();
                }, check).then(function (val){
                    console.log(`Got HTML ${val}`);
                    if (check.val){
                        if (!htmlEq(check.val, val)){
                            notify(check);
                            console.log(`TODO: Found difference! on page ${check.url}`);
                        }
                    }
                    check.val = val;
                });
            });
        }
    });
}

const jobChecker = schedule.scheduleJob("*/1 * * * *", () => {
    console.log("Running jobChecker!");
    db.checks.forEach((check) => {
        if (check.active){
            checkWebsite(check);
        }
    });
});

const saveDB = () => {
    if (process.argv.includes("saveDb")){
        fs.writeFileSync("data/db.json", JSON.stringify(db));
    }
};

process.on("beforeExit", saveDB);

process.on("SIGINT", () => {
    saveDB();
    process.exit();
})