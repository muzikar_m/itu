$(function(){
    $(".toggle-check").click(function() {
        let root = $(this).parent(".check-item");
        console.log(root);
        console.log(root.data("uid"));
        $.get(`/check?id=${$.data($(this).parent(".check-item")[0], "uid")}`).then((data) => {
            data = data[0];
            console.log(data);
            $.ajax({
                url: "/check",
                method: "PATCH",
                data: {
                    id: data.id,
                    active: !data.active
                }
            });
        });
    });
});

